/* ========================================
 *
 * Copyright Eduard Kraemer, 2019
 *
 * ========================================
*/
#ifndef _FLASH_H_
#define _FLASH_H_

#include "OS_Config.h"     
#if USE_OS_FLASH
    
/********************************* includes **********************************/
#include "BaseTypes.h"
    
/***************************** defines / macros ******************************/

/****************************** type definitions *****************************/

/***************************** global variables ******************************/

/************************ externally visible functions ***********************/

    
#ifdef __cplusplus
extern "C"
{
#endif  

void OS_Flash_EraseAll(void);
bool OS_Flash_GetUserSettings(void* pvData, u8 ucSize);
bool OS_Flash_GetSystemSettings(void* pvData, u8 ucSize);
bool OS_Flash_WriteUserSettings(void* pvData, u8 ucSize);
bool OS_Flash_WriteSystemSettings(void* pvData, u8 ucSize);

#ifdef __cplusplus
}
#endif    

#endif //USE_OS_FLASH

#endif //_FLASH_H_
