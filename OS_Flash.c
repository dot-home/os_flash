//********************************************************************************
/*!
\author     
\date       

\file       OS_Flash
\details    This module is just an interface for the HAL-Flash.
*/
            
#include "OS_Flash.h"
#include "HAL_Flash.h"
#include "OS_ErrorDebouncer.h"
/****************************************** Defines ******************************************************/

/****************************************** Variables ****************************************************/

/****************************************** Function prototypes ******************************************/
static bool CheckFlashResult(teSFlashReturn eFlashResult);

/****************************************** local functions *********************************************/
//********************************************************************************
/*!
\author     Kraemer E.
\date       20.01.2019
\brief      Compares the result value from the HAL access and put either a fault into
            the error debouncer or do nothing and return a success.
\return     bReturnVal - True when valid data has been read otherwise false.
\param      eFlashResult - The return value of the flash.
***********************************************************************************/
static bool CheckFlashResult(teSFlashReturn eFlashResult)
{
    bool bReturnVal = false;

    switch (eFlashResult)
    {
        case eSFLASH_Unused:
        case eSFLASH_Success:               bReturnVal = true;                                         break;
        case eSFLASH_CrcReadFault:          OS_ErrorDebouncer_PutErrorInQueue(eFlashCRCInvalid);       break;
        case eSFLASH_Invalid:               OS_ErrorDebouncer_PutErrorInQueue(eInvalidPointerAccess);  break;
        case eSFLASH_DataToBigForSection:   OS_ErrorDebouncer_PutErrorInQueue(eFlashDataToBig);        break;
        case eSFLASH_Protected:             OS_ErrorDebouncer_PutErrorInQueue(eFlashProtected);        break;
        case eSFLASH_InvalidAddress:        OS_ErrorDebouncer_PutErrorInQueue(eFlashAddrInvalid);      break;
        default:                            OS_ErrorDebouncer_PutErrorInQueue(eUnknownReturnValue);    break;
    }

    return bReturnVal;
}



/****************************************** External visible functions **********************************/
//********************************************************************************
/*!
\author     Kraemer E.
\date       20.01.2019
\brief      Just calls the erase all handler.
\return     none
\param      none
***********************************************************************************/
void OS_Flash_EraseAll(void)
{
    HAL_Flash_EraseAll();
}


//********************************************************************************
/*!
\author     Kraemer E.
\date       20.01.2019
\brief      Try to get the user settings from flash.
\return     bReturn - True when valid data has been read otherwise false.
\param      pvData - Pointer on user settings data block for reading
\param      ucSize - Size of user settings data block
***********************************************************************************/
bool OS_Flash_GetUserSettings(void* pvData, u8 ucSize)
{
    teSFlashReturn eUserSettingsReturn = eSFLASH_Invalid;

    if(pvData)
    {
        eUserSettingsReturn = HAL_Flash_GetUserSettings(pvData, ucSize);
    }

    bool bReturn = CheckFlashResult(eUserSettingsReturn);

    return bReturn;
}


//********************************************************************************
/*!
\author     Kraemer E.
\date       21.10.2019
\brief      Try to get the system settings from flash. Checks for correct read.
\return     bReturn - True when valid data has been read otherwise false.
\param      pvData - Pointer on user settings data block for reading
\param      ucSize - Size of user settings data block
***********************************************************************************/
bool OS_Flash_GetSystemSettings(void* pvData, u8 ucSize)
{
    teSFlashReturn eSystemSettingsReturn = eSFLASH_Invalid;

    if(pvData)
    {
        eSystemSettingsReturn = HAL_Flash_GetSystemSettings(pvData, ucSize);
    }

    bool bReturn = CheckFlashResult(eSystemSettingsReturn);

    return bReturn;
}


//********************************************************************************
/*!
\author     Kraemer E.
\date       01.04.2021
\brief      Writes the user settings into  memory.
\return     bReturn - True when valid data has been written otherwise false.
\param      pvData - Pointer on user settings data block to be written
\param      ucSize - Size of user settings data block
***********************************************************************************/
bool OS_Flash_WriteUserSettings(void* pvData, u8 ucSize)
{
    teSFlashReturn eUserSettingsReturn = eSFLASH_Invalid;

    if(pvData)
    {
        eUserSettingsReturn = HAL_Flash_WriteUserSettings(pvData, ucSize);
    }

    bool bReturn = CheckFlashResult(eUserSettingsReturn);

    return bReturn;
}


//********************************************************************************
/*!
\author     Kraemer E.
\date       01.04.2021
\brief      Writes the system settings into  memory.
\return     bReturn - True when valid data has been written otherwise false.
\param      pvData - Pointer on user settings data block to be written
\param      ucSize - Size of user settings data block
***********************************************************************************/
bool OS_Flash_WriteSystemSettings(void* pvData, u8 ucSize)
{
    teSFlashReturn eSystemSettingsReturn = eSFLASH_Invalid;

    if(pvData)
    {
        eSystemSettingsReturn = HAL_Flash_WriteSystemSettings(pvData, ucSize);
    }

    bool bReturn = CheckFlashResult(eSystemSettingsReturn);

    return bReturn;
}
